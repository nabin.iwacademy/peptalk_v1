from django.urls import path
from django.contrib import admin
from .views import HomePage

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',HomePage.as_view(), name="homepage")

]
