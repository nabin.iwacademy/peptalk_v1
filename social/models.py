from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxLengthValidator, RegexValidator

# Create your models here.


class MyProfile(models.Model):
    name = models.CharField(max_length=100)
    user = models.OneToOneField(to=User, on_delete=models.CASCADE)
    age = models.IntegerField(default=18, validators=[MaxLengthValidator(18)])
    address = models.TextField()
    status = models.CharField(max_length=20, default='single', choices=(('single', 'single'), ('married', 'married'), ('separated', 'seperated')))
    gender = models.CharField(max_length=20, default='male', choices=(('male', 'male'), ('female', 'female'), ('others', 'others')))
    phone_no = models.CharField(validators=[RegexValidator("^0?[5-9]{1}\d{9}$")], max_length=15, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    pic = models.ImageField(upload_to="images\\", null=True)

    def __str__(self):
        return self.user


class MyPost(models.Model):
    pic = models.ImageField(upload_to="image\\", null=True)
    subject = models.CharField(max_length=200)
    msg = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    uploaded_by = models.ForeignKey(to=User, on_delete=models.CASCADE)

    def __str__(self):
        return self.subject


class PostComments(models.Model):
    post = models.ForeignKey(to=MyPost, on_delete=models.CASCADE)
    msg = models.TextField()
    commented_by = models.ForeignKey(to=User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    flag = models.CharField(max_length=20, null=True, choices= (("racist", "racist"), ('abbusing','abbusing')))

    def __str__(self):
        return self.msg

class PostLike(models.Model):
    post = models.ForeignKey(MyPost, on_delete=models.CASCADE)
    liked_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.liked_by


class FollowUser(models.Model):
    profile = models.ForeignKey(MyProfile, on_delete=models.CASCADE)
    followed_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return  self.followed_by


