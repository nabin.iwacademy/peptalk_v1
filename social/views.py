from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from .models import MyProfile, FollowUser, PostLike,PostComments, MyPost
from django.views.generic.detail import DetailView
from django.db.models import Q
from  django.views.generic.edit import UpdateView, CreateView

# Create your views here.


class HomePage(TemplateView):
    template_name = "social/home.html"

