from django.contrib import admin
from social.models import MyProfile, MyPost, FollowUser, PostComments, PostLike
from django.contrib.admin.options import ModelAdmin

# Register your models here.


class FollowUserAdmin(ModelAdmin):
    list_display = ['profile', 'followed_by']
    search_fields = ['profile', 'followed_by']
    list_filter = ['profile', 'followed_by']


class MyPostAdmin(ModelAdmin):
    list_display = ['subject', 'msg', 'created_date', 'uploaded_by']
    search_fields = ['subject', 'msg', 'created_date', 'uploaded_by']
    list_filter = ['created_date', 'uploaded_by']


class MyProfileAdmin(ModelAdmin):
    list_display = ['name', 'user', 'age', 'address', 'status', 'gender', 'phone_no', 'description',]
    search_fields = ['name', 'user', 'address','phone_no',]
    list_filter = ['name', 'user', 'address','phone_no',]


class PostCommentsAdmin(ModelAdmin):
    list_display = ['post', 'msg', 'commented_by', 'created_date', 'flag',]
    search_fields = ['post', 'msg', 'commented_by',]
    list_filter = ['post', 'msg', 'commented_by',]


class PostLikeAdmin(ModelAdmin):
    list_display = ['post', 'liked_by', 'created_date',]
    search_fields = ['post', 'liked_by', 'created_date',]
    list_filter = ['post', 'liked_by', 'created_date',]


admin.site.register(FollowUser, FollowUserAdmin)
admin.site.register(MyPost, MyPostAdmin)
admin.site.register(MyProfile, MyProfileAdmin)
admin.site.register(PostComments, PostCommentsAdmin)
admin.site.register(PostLike, PostLikeAdmin)